# -*- coding: utf-8 -*-
import csv
import os
import re
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ims.settings")
django.setup()
from shops.models import shop, category
from django.utils import timezone

'''
Prosze edytować lokalizacje plików do zaimportowania
'''
CATEGORIES_CSV_NAME = 'categories.csv'
SHOPS_CSV_NAME = 'shops.csv'

def fix_isoformat_date(date_string):
    '''
    W pliku csv sa daty w formacie iso np: "2014-11-26 12:27:35.666588 +01:00"
    Wg normy iso 8601 nie ma spacji przed stefa czasowa, a przynajmniej
    python tego nie chce przyjąć wiec wycinam tą spację. Tak samo gdy nie ma w
    ogóle strefy czasowej to dodajemy Z(czyli UTC)
    '''
    ds = date_string.replace(' +','+')
    if re.match('^[0-9\.\-: ]+$',ds):
        ds += 'Z'
    return ds

def import_categories(file_location):
    categories_csv = csv.DictReader(open(file_location))
    category.objects.bulk_create([category( \
        name=c['name'], \
        visible=c['visible']=='true', \
        import_id=int(c['id']) \
        ) for c in categories_csv])

def import_shops(file_location):
    shops_csv = csv.DictReader(open(file_location))
    shop.objects.bulk_create([shop( \
        name = s['name'], \
        description = s['description'], \
        main_category = category.objects.filter(import_id=int(s['category_id'])).first(), \
        level = int(s['level']), \
        lottery = s['lottery'] == 'true', \
        created_at = fix_isoformat_date(s['created_at']), \
        updated_at = fix_isoformat_date(s['updated_at']), \
        ) for s in shops_csv])

if __name__ == "__main__":
    print('Importowanie informacji z pliku CSV do bazy')
    print('Zaimportuje kategorie z "{0}" oraz sklepy z "{1}"'.format(CATEGORIES_CSV_NAME,SHOPS_CSV_NAME))
    input('Przygotuj pliki następnie Enter aby zaimportować')
    import_categories(CATEGORIES_CSV_NAME)
    import_shops(SHOPS_CSV_NAME)
