from django.contrib import admin

from .models import shop, category

class shopAdmin(admin.ModelAdmin):
    list_display = ('name','id','logomin','main_category','level','lottery','description','created_at','updated_at')
    list_editable = ('description',)
    search_fields = ('name','description',)
    list_filter = ('main_category','level','lottery','created_at','updated_at')
    ordering = ('-id',)
    list_per_page = 50
    readonly_fields = ('created_at', 'updated_at',)

class categoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'id', 'import_id', 'visible')
    search_fields = ('name',)
    list_filter = ('visible',)
    ordering = ('-id',)
    list_per_page = 50

admin.site.register(shop, shopAdmin)
admin.site.register(category, categoryAdmin)
