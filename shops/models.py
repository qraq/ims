# -*- coding: utf-8 -*-
from django.db import models

from django.utils import timezone

from uuid import uuid4

def imgnewname(instance, filename):
    return 'img/{0}.jpg'.format(uuid4())

class shop(models.Model):
    name = models.CharField('Nazwa', max_length=255)
    description = models.TextField('Opis')
    logo = models.ImageField('Logo', blank=True, upload_to=imgnewname)
    main_category = models.ForeignKey('category', verbose_name='Główna kategoria', related_name='kat')
    categories = models.ManyToManyField('category', verbose_name='Kategorie dodatkowe', blank=True)
    level = models.IntegerField('Poziom')
    lottery = models.BooleanField('Loteria', help_text='Uczestniczy w loterii')
    created_at = models.DateTimeField('Data utworzenia', auto_now_add=True)
    updated_at = models.DateTimeField('Data modyfikacji', auto_now=True)

    class Meta:
        verbose_name = 'Sklep'
        verbose_name_plural = 'Sklepy'

    def __unicode__(self):
        return str(self.name)

    def __str__(self):
        return str(self.name)

    def logomin(self):
        if self.logo:
            return '<img src="%s" style="width: 100px; height: 100px;">' % self.logo.url
        else:
            return 'Brak'
    logomin.allow_tags = True
    logomin.short_description = 'Logo'

class category(models.Model):
    name = models.CharField('Nazwa', max_length=255)
    visible = models.BooleanField('Widoczny')
    import_id = models.IntegerField('Import ID', default=0)

    class Meta:
        verbose_name = 'Kategoria'
        verbose_name_plural = 'Kategorie'

    def __unicode__(self):
        return str(self.name)

    def __str__(self):
        return str(self.name)
