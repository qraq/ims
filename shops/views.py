from django.shortcuts import render, redirect

from .models import shop, category
from .forms import add_shop_form

from django.http import HttpResponse, Http404

from django.contrib.auth.decorators import login_required

from rest_framework import viewsets
from .serializers import shopSerializer, categorySerializer

class shopViewSet(viewsets.ModelViewSet):
    queryset = shop.objects.all().order_by('-id')
    serializer_class = shopSerializer

class categoryViewSet(viewsets.ModelViewSet):
    queryset = category.objects.all().order_by('-id')
    serializer_class = categorySerializer

def show_all_shops(request):
    try:
        all_shops = shop.objects.all()
    except:
        raise Http404

    context = {'shops': all_shops}
    return render(request, 'all_shops.html', context)

def show_shop(request, shopid):
    try:
        singleshop = shop.objects.get(id=shopid)
    except:
        raise Http404

    context = {'singleshop': singleshop}
    return render(request, 'single_shop.html', context)

@login_required
def add_shop(request):
    if request.method != 'POST':
        form = add_shop_form()
    else:
        form = add_shop_form(request.POST, request.FILES)
        if form.is_valid():
            shop_data = form.save()
            return redirect('/shop/{0}/'.format(shop_data.id))

    context = {'form': form}
    return render(request, 'add_shop.html', context)

@login_required
def delete_shop(request, shopid):
    try:
        singleshop = shop.objects.get(id=shopid)
    except:
        raise Http404

    if request.method == 'POST' and 'confirm' in request.POST:
        singleshop.delete()
        return redirect('/shop/all')

    context = {'singleshop': singleshop}
    return render(request, 'delete_shop.html', context)
