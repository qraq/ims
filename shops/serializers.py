from rest_framework import serializers
from .models import shop, category

class shopSerializer(serializers.HyperlinkedModelSerializer):
    created_at = serializers.ReadOnlyField(read_only=True)
    updated_at = serializers.ReadOnlyField(read_only=True)
    lottery = serializers.BooleanField(required=True)
    class Meta:
        model = shop
        fields = ('id','name', 'description', 'logo','main_category','categories','level','lottery','created_at','updated_at')

class categorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = category
        fields = ('id','name','visible','import_id')
