from django import forms
from django.forms import ModelForm

from .models import shop

class add_shop_form(ModelForm):
    class Meta:
        model = shop
        fields = ['name','description','logo','main_category','categories','level','lottery']

'''
CATEGORY_CHOICES = (
    (1, 'asdassa'),
)

# jednak korzystamy z szybszej metody :)
class add_shop_form(forms.Form):
    name = forms.CharField(label='Nazwa', max_length=255)
    description = forms.CharField(label='Opis', widget=forms.Textarea)
    logo = forms.ImageField(label='Logo', required=False)
    main_category = forms.ChoiceField(label='Kategoria główna', choices=CATEGORY_CHOICES)
    categories = forms.MultipleChoiceField(label='Pozostałe kategorie', choices=CATEGORY_CHOICES, required=False)
    level = forms.IntegerField(label='Poziom')
    lottery = forms.BooleanField(label='Uczestniczy w loterii', required=False)
'''
