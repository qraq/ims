"""ims URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views

from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.base import RedirectView

from shops.views import show_all_shops, show_shop, add_shop, delete_shop, \
    shopViewSet, categoryViewSet

from rest_framework import routers
router = routers.DefaultRouter()
router.register(r'shop', shopViewSet)
router.register(r'category', categoryViewSet)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/login/$', auth_views.login),
    url(r'^accounts/logout/$', auth_views.logout),

    url(r'^$', RedirectView.as_view(url='/shop/all', permanent=False)),

    url(r'^shop/all/$', show_all_shops), #wszystkie sklepy
    url(r'^shop/(?P<shopid>[0-9]+)/$', show_shop),

    url(r'^shop/add/$', add_shop),
    url(r'^shop/(?P<shopid>[0-9]+)/delete$', delete_shop),

    url(r'^api/', include(router.urls)),
    url(r'^api/api-auth/', include('rest_framework.urls', namespace='rest_framework'))

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) #pliki statyczne
